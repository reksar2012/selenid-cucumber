package ru.yandex.runner;
import com.codeborne.selenide.Configuration;
import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;
import javafx.beans.property.SetProperty;
import org.testng.annotations.*;
import ru.yandex.steps.LoginForm;
import ru.yandex.steps.MainPage;

import static com.codeborne.selenide.Selenide.open;
import static com.codeborne.selenide.Selenide.screenshot;
import static com.codeborne.selenide.Selenide.sleep;

@CucumberOptions(features = "src/test/java/ru/yandex/features", glue = "src\\test\\java\\ru\\yandex\\steps\\YandexStep.java")
public class test1 extends AbstractTestNGCucumberTests {

}
