package ru.yandex.steps;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

public class LoginForm {
    public SelenideElement userNameInput=$(By.name("userName"));
    public SelenideElement pwdInput=$(By.name("password"));
    public void SetLoginAndPassword(String login,String password){
        userNameInput.val(login);
        pwdInput.val(password);
    }
}
