package ru.yandex.steps;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;


import static com.codeborne.selenide.Selenide.*;

public class MainPage {
    LoginForm loginForm=new LoginForm();
    public SelenideElement loginButton=$(By.className("main-nav__btn"));

    public  void Open(String url){
        open(url);
    }
    public void ClickOnLoginButton(){
        loginButton.click();
    }


    public void ClickOnSubmitButtonOnLoginForm() {
    }
}
