package ru.yandex.steps;

import com.codeborne.selenide.Condition;
import cucumber.api.CucumberOptions;
import cucumber.api.java.en.*;

import static com.codeborne.selenide.Selectors.byName;
import static com.codeborne.selenide.Selectors.byText;
import static com.codeborne.selenide.Selectors.byValue;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;
import static com.codeborne.selenide.Selenide.sleep;


public class YandexStep {
    MainPage riskMainPage;
    @When("^press button with text \"([^\"]*)\"$")
    public void press(String button)
    {
        $(byText(button)).waitUntil(Condition.visible, 15000).click();
    }
    @And("^type to input with name \"([^\"]*)\" text: \"([^\"]*)\"$")
    public void typeToInputWithNameText(String input, String text)
    {
        sleep(1000);
        $(byName(input)).sendKeys(text);
    }
    @And("^press element with value \"([^\"]*)\"$")
    public void pressButton(String button)
    {
        $(byValue(button)).waitUntil(Condition.visible, 15000).click();
    }

    @Given("^open ([^\"]*)$")
    public void typeToInputWithNameText(String url)
    {
        open(url+"adfzd");
    }
}
